<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahgoza extends Model
{
    //
    protected $table = 'mahgoza';
//    protected $fillable = [
//        'mahgoza_id', 'mahgoza_number',
//    ];
    protected $fillable = [
         'mahgoza_number','shehada_id',
    ];

        public function shows(){
            return $this->belongsToMany(Show::class);
        }
    public function shehada(){
        return $this->belongsTo(Shehada::class);
    }
}
