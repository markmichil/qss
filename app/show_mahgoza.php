<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class show_mahgoza extends Model
{
    //
    protected $table = 'mahgoza_show';

    public  $timestamps =false;

    protected $fillable = [
        'shahda_mahgoza_id', 'Show_id','mahgoza_id',
    ];
//
//    public function show(){
//        return $this->hasMany(Show::class);
//    }
//    public function mahgoza(){
//        return $this->hasMany(Mahgoza::class);
//    }
}
