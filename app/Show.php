<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Show extends Model
{
 protected $table = 'show';
//    protected $fillable = [
//        'id_show', 'name_show',
//    ];
 protected $fillable = [
         'name_show',
    ];
    public function mahgoza(){
        return $this->belongsToMany(Mahgoza::class);
    }
}
