<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shehada extends Model
{
    //

    protected $table= 'shehada';
    protected $fillable = [
        'name','policy','status','finsih','emergency','problem','customer_id',
    ];

    public function customer(){
        return $this->belongsTo('App\Mostkles');
    }
    public function mahgoza(){
        return $this->hasMany('App\Mahgoza');
    }
}
