<?php

namespace App\Http\Controllers\user;
use  App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Closure;
use Illuminate\Support\Facades\Auth;

class UserHomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');


    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        return view('user.index');
    }

    public function  mostkles(){


        return view("mostkles");
    }

    public function  mostklesById(){


        return view("mostklesById");
    }

    public function  shehada(){


        return view("shehada");
    }

    public function  finish(){


        return view("finish");
    }

    public function  still(){


        return view("still");
    }

    public function  returnn(){


        return view("return");
    }
}
