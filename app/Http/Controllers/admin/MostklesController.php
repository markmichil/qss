<?php
namespace App\Http\Controllers\admin;
use  App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mostkles;
use Closure;
use Illuminate\Support\Facades\Auth;
class MostklesController extends Controller
{
    public function __construct()
{
    $this->middleware('admin');

}


    public function  newMostkles(){


        return view("mostkles/new");
    }

    public function  addMostkles(Request $request){


        $show = Mostkles::create(['customer_name' => $request['name']]);


        return $this->allMostkles();
    }

    public function  allMostkles(){
        $show = Mostkles::paginate(2);

        return view("mostkles/allShow")->with('show',$show);
    }

    public function  updateMostkles(Request $request){

        $show = Mostkles::where('id',$request->id)->first();
//        dd($show);
        return view("mostkles/update")
            ->with('id_show',$request->id)
            ->with('name',$show->customer_name);
    }



    public function  DoupdateMostkles(Request $request){

        $show = Mostkles::where('id',$request->id)->first();


//dd($show);
        $show->customer_name      = $request->customer_name;





        $show->save();
        return   $this->allMostkles();

    }






}
