<?php

namespace App\Http\Controllers\admin;

use App\show_mahgoza;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


use App\Mahgoza;
use App\show;
use App\Shehada;



class MahgozaController extends Controller
{

     public function __construct()
            {

                $this->middleware('admin');

            }


    public function newMahgoza(){

         $shehades = Shehada::all();
        return view("mahgoza/new")->with('shehades',$shehades);
    }


    public function addMahgoza (Request $request){
//dd($request->mahgoza_number);

        $show = Mahgoza::create(['mahgoza_number' => $request->mahgoza_number,
            'shehada_id'=>$request->shehada_id]);

        return $this->customeMahgoza();
    }

    public function mahgozas (){
        $mahgozas = Mahgoza::paginate(10);

        return view("mahgoza/mahgoza")->with('mahgozas',$mahgozas);
    }

    public function updateMahgozasShehada (Request $request){
//      dd($request->id);
     $mahgoza = Mahgoza::find($request->id);
     $shehades = Shehada::all();
//     dd($mahgoza->shehada->name);
        return view("mahgoza/updateSheda")->with('shehades',$shehades)
            ->with('mahgoza',$mahgoza);
    }

    public function DoupdateMahgozasShehada (Request $request){
//      dd($request->id);
     $mahgoza = Mahgoza::find($request->id);
     $mahgoza->mahgoza_number = $request->mahgoza_number;
     $mahgoza->shehada_id = $request->shehada_id;
     $mahgoza->save();

     return redirect('mahgozas');

    }

    public function updateMahgoza(Request $request){
        $mahgoza = Mahgoza::find($request->id);

        $show = show::all();
         return view('mahgoza/updatemahgoza')
             ->with('shows',$show)
             ->with('mahgozas',$mahgoza);
    }

    public function  DoupdateMahgoza(Request $request){

       $mahgoza = Mahgoza::find($request->id);

        $mahgoza->mahgoza_number = $request->mahgoza_number;

        $mahgoza->shows()->sync($request->shows);
        $mahgoza->save;

        return redirect(route('allmahgoza'));

    }

    public function  customeMahgoza(){


        $mahgoza = Mahgoza::all();
        $show = show::all();

        return view("mahgoza/customeMahgoza")
            ->with('mahgozas',$mahgoza)
            ->with('shows',$show);
    }



    public function  DocustomeMahgoza(Request $request){

        $shows = $request->shows;
        $maghozas = $request->mahgoza;


       foreach ($shows as $show){
           $new = new show_mahgoza();
           $new->Show_id = $show;
           $new->mahgoza_id = $maghozas;
           $new->save();

       }


        return $this->all();

    }

    public function all(){

$mahgozas = Mahgoza::paginate(4);
//dd($mahgoza[0]->shows[0]->name_show);
        return view("mahgoza/allShow")->with('mahgozas',$mahgozas);
    }


}
