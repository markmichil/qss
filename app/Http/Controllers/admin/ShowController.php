<?php



namespace App\Http\Controllers\admin;
use  App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Show;
use Closure;
use Illuminate\Support\Facades\Auth;
class ShowController extends Controller
{   public function __construct()
{

    $this->middleware('admin');

}


    public function  newShow(){


        return view("3ard/new");
    }

    public function  addShow(Request $request){


        $show = Show::create(['name_show' => $request['name_show']]);


        return $this->allShow();
    }

    public function  allShow(){
        $show = Show::paginate(5);


        return view("3ard/allShow")->with('shows',$show);
    }

    public function  updateShow(Request $request){

        $show = Show::where('id',$request->id)->first();
//        dd($show);
        return view("3ard/update")
            ->with('id_show',$request->id)
            ->with('name',$show->name_show);
    }



    public function  DoupdateShow(Request $request){

        $show = Show::where('id',$request->id_show)->first();


//dd($show);
        $show->name_show      = $request->name_show;





        $show->save();
        return   $this->allShow();

    }






}
