<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\User;

use Illuminate\Support\Facades\Hash;


use Closure;
use Illuminate\Support\Facades\Auth;


class MowazafController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('admin');

    }


    public function  newMowazaf(){


        return view("mowazaf/new");
    }

    protected function create(Request $request  )
    {
//        dd($data);
       User::create([
            'name' =>  $request ['name'],
            'email' => $request  ['email'],
            'phone' => $request ['phone'],
            'password' => Hash::make( $request ['password']),

            'admin' => $request ['type'],
        ]);

        return redirect(url('job'));
    }


    public function  allMowazaf(){


        return view("mowazaf/allShow");
    }
    public function  custome(){


        return view("mowazaf/custome");
    }
    public function  job(){


        return view("mowazaf/job");
    }

}
