<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Mostkles;
use App\Shehada;


class ShehadaController extends Controller

{
    public function __construct()
        {

            $this->middleware('admin');

        }

    public function  newShehada(){

        $mostkles = Mostkles::all();


        return view("shehada/new")->with('mostkles',$mostkles);
    }
    public function  addShehada(Request $request){

        $shehada = new Shehada();
        $shehada->name = $request->name;
        $shehada->customer_id = $request->customer_id;
        $shehada->policy = $request->policy;
        $shehada->save();
     return redirect(route('newMahgoza'));
    }

        public function  allShehada(){

            $shehadas = Shehada::paginate(5);
//           dd( $shehadas[0]->mahgoza[0]->mahgoza_number);
//

            return view("shehada/allShow")->with('mmm',$shehadas);
             }



}
