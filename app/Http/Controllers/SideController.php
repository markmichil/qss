<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SideController extends Controller
{

    public function __construct()
    {

        $this->middleware('admin');

    }


//    public function  newShow(){
//
//
//        return view("3ard/new");
//    }
//    public function  allShow(){
//
//
//        return view("3ard/allShow");
//    }

    public function  newMostkles(){


        return view("mostkles/new");
    }
    public function  allMostkles(){


        return view("mostkles/allShow");
    }

//    public function  newShehada(){
//
//
//        return view("shehada/new");
//    }
//    public function  allShehada(){
//
//
//        return view("shehada/allShow");
//    }
//    public function  customeShehada(){
//
//
//        return view("shehada/customeShehada");
//    }

    public function  newMowazaf(){


        return view("mowazaf/new");
    }
    public function  allMowazaf(){


        return view("mowazaf/allShow");
    }
    public function  custome(){


        return view("mowazaf/custome");
    }
    public function  job(){


        return view("mowazaf/job");
    }

//    public function  newMahgoza(){
//
//
//        return view("mahgoza/new");
//    }
//    public function  customeMahgoza(){
//
//
//        return view("mahgoza/customeMahgoza");
//    }

    public function  notRecive(){
        return view("report/notRecive");
    } public function  working(){
        return view("report/working");
    }


}
