<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Closure;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('admin');



    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('index');
    }

    public function  mostkles(){


        return view("mostkles");
    }

    public function  mostklesById(){


        return view("mostklesById");
    }

    public function  shehada(){


        return view("shehada");
    }

    public function  finish(){


        return view("finish");
    }

    public function  still(){


        return view("still");
    }

    public function  returnn(){


        return view("return");
    }


    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/login');
    }

    protected function guard()
    {
        return Auth::guard();
    }

}
