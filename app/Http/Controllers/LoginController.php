<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class LoginController extends Controller
{
    public function login(Request $request){
        if(Auth::attempt([
            'email'=>$request->email,
            'password'=>$request->password]))

        {
            $user = User::where('email',$request->email)->first();
            if($user->IsAdmin()){
//                dd($user->IsAdmin());
                Auth::guard('admin');
                return redirect()->route('dashboard');
            }else

//                dd($user->admin);
//                dd($user->IsAdmin());
                Auth::guard('web');
                return redirect()->route('user');
            }
//else{
//                dd($user->admin);
//                Auth::guard('web');
//                return redirect()->route('dhl');
//            }
        else{
            return redirect()->back();
        }
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }

    protected function guard()
    {
        return Auth::guard();
    }

}



