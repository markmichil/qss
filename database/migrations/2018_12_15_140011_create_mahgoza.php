<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMahgoza extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahgoza', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('mahgoza_number');



            $table->integer('shehada_id')->unsigned();
            $table->foreign('shehada_id')->references('id')->on('shehada');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahgoza');
    }
}
