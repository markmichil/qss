<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShehada extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shehada', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('policy');
            $table->boolean('status')->default(1);
            $table->boolean('finsih')->default(0);
            $table->boolean('emergency')->default(0);
            $table->boolean('problem')->default(0);
            $table->integer('customer_id')->unsigned();
            $table->foreign('customer_id')->references('id')->on('customer');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shehada');
    }
}
