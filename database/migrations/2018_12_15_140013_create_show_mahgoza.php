<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowMahgoza extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mahgoza_show', function (Blueprint $table) {
            $table->increments('shahda_mahgoza_id');
            $table->integer('Show_id')->unsigned();
            $table->foreign('Show_id')->references('id')->on('show');
            $table->integer('mahgoza_id')->unsigned();
            $table->foreign('mahgoza_id')->references('id')->on('mahgoza');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mahgoza_show');
    }
}
