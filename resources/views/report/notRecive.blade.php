@extends('../front')
@section('container')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <h1 style="color: #0088cc; text-align: center">الشهادات التى لم تستلم </h1>
                    </header>
                    <table   class="table table-striped table-advance table-hover">
                        <thead>
                        <tr>
                            <th><i class="icon-bullhorn"></i> {{__('key.Nshehada')}}</th>
                            <th class="hidden-phone"><i class="icon-question-sign"></i> {{__('key.police')}}</th>

                            <th><i class="icon-bookmark"></i> {{__('key.mostkles')}}</th>
                            <th><i class=" icon-edit"></i>الموظف</th>
                            <th><i class=" icon-edit"></i>التعليق</th>




                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td>
                                <a href="#">
                                    جى ايه
                                </a>
                            </td>
                            <td class="hidden-phone">8600680642</td>
                            <td>محمد DHL </td>
                            <td><span class="label label-warning label-mini">أحمد</span></td>
                          <td> لأ يوجد تعليق</td>
                        </tr>
                        <tr>
                            <td>
                                <a href="#">
                                    جى ايه
                                </a>
                            </td>
                            <td class="hidden-phone">8600680642</td>
                            <td>محمد DHL </td>
                            <td><span class="label label-warning label-mini">محمد</span></td>
                          <td>
                              <button class="btn btn-success btn-xs"><i class="icon-edit"> إظهار التعليق</i></button>
                          </td>

                        </tr>





                        </tbody>
                    </table>
                </section>
            </div>
        </div>

        {{---------------------------}}

    </section>
</section>
<!--main content end-->


@stop