@extends('admin.front')

@section('container')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!--state overview start-->
        <div class="row state-overview">
            <div class="col-lg-12 col-sm-12">

                <section class="panel">
                    <a href="{{url('/mostkles')}}">
                        <div class="symbol terques">
                            <i class="icon-user"></i>
                            <h4 style="color:white">{{__('key.mostkles')}}</h4>
                        </div>
                        <div class="value">
                            <h1 class="count">
                                0
                            </h1>
                            <p>{{__('key.mostkles')}}</p>
                        </div>
                    </a>
                </section>

            </div>

            <div class="col-lg-12 col-sm-12">
                <section class="panel">
                    <a href="{{url('/shehada')}}">
                        <div class="symbol red">
                            <i class="icon-tags"></i>
                            <h4 style="color:white">{{__('key.shehada')}}</h4>
                        </div>
                        <div class="value">
                            <h1 class=" count2">
                                0
                            </h1>
                            <p>{{__('key.shehada')}}</p>
                        </div>
                    </a>
                </section>
            </div>

            <div class="col-lg-12 col-sm-12">
                <section class="panel">
                    <a href="{{url('/finish')}}">
                        <div class="symbol yellow">
                            <i class="icon-file-text-alt"></i>
                            <h4 style="color:white">{{__('key.finish')}}</h4>
                        </div>
                        <div class="value">
                            <h1 class=" count3">
                                0
                            </h1>
                            <p>{{__('key.finish')}}</p>
                        </div>
                    </a>
                </section>
            </div>


            <div class="col-lg-12 col-sm-12">
                <section class="panel">
                    <a href="{{url('/still')}}">
                        <div class="symbol blue">
                            <i class="icon-list-alt"></i>
                            <h4 style="color:white">{{__('key.still')}}</h4>
                        </div>
                        <div class="value">
                            <h1 class=" count4">
                                0
                            </h1>
                            <p>{{__('key.still')}}</p>
                        </div>
                    </a>
                </section>
            </div>

            <div class="col-lg-12 col-sm-12">
                <section class="panel">
                    <a href="{{url('/return')}}">
                        <div class="symbol terques">
                            <i class="icon-repeat"></i>
                            <h4 style="color:white">{{__('key.return')}}</h4>
                        </div>
                        <div class="value">
                            <h1 class="count4">
                                0
                            </h1>
                            <p>{{__('key.return')}}</p>
                        </div>
                    </a>
                </section>
            </div>
        </div>
        <!--state overview end-->

        <div class="row">

        </div>


    </section>
</section>
<!--main content end-->


@stop