<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="{{asset('img')}}/mark.ico">

    <title>Quick Shiping Services</title>

    <!-- Bootstrap core CSS -->
    <link href="{{asset('css')}}/bootstrap.min.css" rel="stylesheet">
    <link href="{{asset('css')}}/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="{{asset('assets')}}/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="{{asset('assets')}}/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="{{asset('css')}}/owl.carousel.css" type="text/css">
    <!-- Custom styles for this template -->
    <link href="{{asset('css')}}/style.css" rel="stylesheet">
    <link href="{{asset('css')}}/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="{{asset('js')}}/html5shiv.js"></script>
      <script src="{{asset('js')}}/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

  <section id="container" >
      <!--header start-->
      <header class="header white-bg">

            <div class="sidebar-toggle-box">
                <div data-original-title="Toggle Navigation" data-placement="right" class="icon-reorder tooltips">
                    <!--logo start-->

                    {{--<a href="{{url('/')}}" class="logo">Q <span> S S</span></a><br>--}}
                    <a href="{{url('/')}}" class="logo" ></a><br>
                    <!--logo end-->
                </div>

                <img style="width: 200px; " src="{{url("NAGWA")}}/logo.png">
                {{--<img style="width: 200px; " src="{{url("NAGWA")}}/logo1.png">--}}
            </div>



            <div class="top-nav ">
                <!--search & user info start-->
                <ul class="nav pull-right top-menu">


                    <!-- user login dropdown start-->
                    <li class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <img alt="" src="{{asset('img')}}/avatar1_small.jpg">
                            <span class="username">NAGWA ADMIN</span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu extended logout">
                            <div class="log-arrow-up"></div>

                            <li>

                                <a class=" icon-key dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{--{{ __('Logout') }}--}}
                                    تسجيل خروج
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>



                                {{--<a href="{{url("/logout")}}"><i class="icon-key"></i> Log Out</a></li>--}}
                        </ul>
                    </li>
                    <!-- user login dropdown end -->
                    <!-- notification dropdown start-->
                    <li id="header_notification_bar" class="dropdown">
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">

                            <i class="icon-bell-alt"></i>
                            <span class="badge bg-warning">7</span>
                        </a>
                        <ul class="dropdown-menu extended notification">
                            <div class="notify-arrow notify-arrow-yellow"></div>
                            <li>
                                <p class="yellow">You have 7 new notifications</p>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-danger"><i class="icon-bolt"></i></span>
                                    Server #3 overloaded.
                                    <span class="small italic">34 mins</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-warning"><i class="icon-bell"></i></span>
                                    Server #10 not respoding.
                                    <span class="small italic">1 Hours</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-danger"><i class="icon-bolt"></i></span>
                                    Database overloaded 24%.
                                    <span class="small italic">4 hrs</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-success"><i class="icon-plus"></i></span>
                                    New user registered.
                                    <span class="small italic">Just now</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="label label-info"><i class="icon-bullhorn"></i></span>
                                    Application error.
                                    <span class="small italic">10 mins</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">See all notifications</a>
                            </li>
                        </ul>
                    </li>
                    <!-- notification dropdown end -->

                </ul>
                <!--search & user info end-->
            </div>
        </header>
      <!--header end-->
      <!--sidebar start-->
      <aside>
          <div id="sidebar"  class="nav-collapse ">
              <!-- sidebar menu start-->
              <ul class="sidebar-menu" id="nav-accordion">


                  <li>
                      <a class="active" href="{{url('/')}}">
                          <i class="icon-dashboard"></i>
                          <span>{{__('key.dashboard')}}</span>
                      </a>
                  </li>



                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="icon-laptop"></i>
                          <span>{{__('key.show')}}</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{url("/newShow")}}">{{__('key.addshow')}}</a></li>
                          <li><a  href="{{url("/allShow")}}">{{__('key.sshow')}}</a></li>

                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="icon-laptop"></i>
                          <span>{{__('key.mostkles')}}</span>
                      </a>
                      <ul class="sub">
                          <li><a href="{{url("/newMostkles")}}">{{__('key.amostkles')}}</a></li>
                          <li><a href="{{url("/allMostkles")}}">{{__('key.smostkles')}}</a></li>

                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="icon-laptop"></i>
                          <span>{{__('key.Mahgoza')}}</span>
                      </a>
                      <ul class="sub">
                          <li><a href="{{url("/newMahgoza")}}">{{__('key.aMahgoza')}}</a></li>
                          <li><a  href="{{url("/customeMahgoza")}}">تخصيص المحجوزة للجهة</a></li>


                      </ul>
                  </li>

                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="icon-laptop"></i>
                          <span>{{__('key.shehada')}}</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{url("/newShehada")}}">{{__('key.ashehada')}}</a></li>
                          <li><a  href="{{url("/allShehada")}}">{{__('key.sshehada')}}</a></li>
                          <li><a  href="{{url("/customeShehada")}}">تخصيص رقم المحجوزة للشهادة</a></li>
                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="icon-laptop"></i>
                          <span>{{__('key.mozaf')}}</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{url("/newMowazaf")}}">{{__('key.amozaf')}}</a></li>
                          <li><a  href="{{url("/allMowazaf")}}">{{__('key.smozaf')}}</a></li>
                          <li><a  href="{{url("/custome")}}">تخصيص الموظف للجهة</a></li>
                          <li><a  href="{{url("/job")}}">تخصيص المحجوزة للموظف</a></li>

                      </ul>
                  </li>
                  <li class="sub-menu">
                      <a href="javascript:;" >
                          <i class="icon-laptop"></i>
                          <span>تقارير</span>
                      </a>
                      <ul class="sub">
                          <li><a  href="{{url("/notRecive")}}">الشهادات لم تستلم</a></li>
                          <li><a  href="{{url("/working")}}">الشهادات قيد التشغيل</a></li>


                      </ul>
                  </li>



              </ul>
              <!-- sidebar menu end-->
          </div>
      </aside>
      <!--sidebar end-->






      <!--main content start-->
     @yield('container')
      <!--main content end-->









      <!--footer start-->
      <footer class="site-footer">
          <div class="text-center">
              2018 &copy; QSS by <a style="color: #57c8f2;" href="http://www.quick-apps.com">QUCIK-APPS</a>.
              <a href="#" class="go-top">
                  <i class="icon-angle-up"></i>
              </a>
          </div>
      </footer>
      <!--footer end-->
  </section>

    <!-- js placed at the end of the document so the pages load faster -->
    <script src="{{asset('js')}}/jquery.js"></script>
    <script src="{{asset('js')}}/jquery-1.8.3.min.js"></script>
    <script src="{{asset('js')}}/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="{{asset('js')}}/jquery.dcjqaccordion.2.7.js"></script>
    <script class="include" type="text/javascript" src="{{asset('js')}}/jquery.dcjqaccordion.2.7.js"></script>
    <script src="{{asset('js')}}/jquery.scrollTo.min.js"></script>
    <script src="{{asset('js')}}/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="{{asset('js')}}/jquery.sparkline.js" type="text/javascript"></script>
    <script src="{{asset('assets')}}/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="{{asset('js')}}/owl.carousel.js" ></script>
    <script src="{{asset('js')}}/jquery.customSelect.min.js" ></script>
    <script src="{{asset('js')}}/respond.min.js" ></script>



    <!--common script for all pages-->
    <script src="{{asset('js')}}/common-scripts.js"></script>

    <!--script for this page-->
    <script src="{{asset('js')}}/sparkline-chart.js"></script>
    <script src="{{asset('js')}}/easy-pie-chart.js"></script>
    <script src="{{asset('js')}}/count.js"></script>

  <script>

      //owl carousel

      $(document).ready(function() {
          $("#owl-demo").owlCarousel({
              navigation : true,
              slideSpeed : 300,
              paginationSpeed : 400,
              singleItem : true,
			  autoPlay:true

          });
      });

      //custom select box

      $(function(){
          $('select.styled').customSelect();
      });

  </script>

  </body>
</html>
