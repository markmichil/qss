@extends('../front')
@section('container')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                       <h1 style="color: #0088cc; text-align: center">{{__('key.show')}}</h1>
                    </header>
                    <table   class="table table-striped table-advance table-hover">
                        <thead>
                        <tr>
                            <th><i class="icon-bullhorn"></i> أسم الجهة </th>

                            <th><i class=" icon-edit"></i> كنترول </th>


                        </tr>
                        </thead>
                        <tbody>
@if(count($shows)>0)
@foreach($shows as $sh)
                        <tr>
                            <td>
                                <a href="#">
                                  {{$sh->name_show}}
                                </a>

                            </td>
                            <td>


                                <a href="{{url('updateShow')}}/{{$sh->id}}" class="btn btn-primary btn-xs"><i class="icon-pencil">تعديل</i></a>




                            </td>
                        </tr>
                        @endforeach
    @else
    <tr>
        <td>لا يوجد بيانات أدخل جهة عرض اولا</td>
        <td>لا يوجد بيانات</td>
    </tr>

    @endif


                        </tbody>
                    </table>
                </section>
            </div>
        </div>
        {{ $shows->links() }}
        {{---------------------------}}

    </section>
</section>
<!--main content end-->


@stop