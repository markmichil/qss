@extends('../front')
@section('container')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!--state overview start-->
            {{--<div class="row state-overview">--}}
                {{--<div class="col-lg-12 col-sm-12">--}}

                    {{--<section class="panel">--}}


                        <div class="row state-overview" dir="rtl">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                    إضافة جهة العرض
                                    </header>
                                    <div class="panel-body">
                                        <form role="form"  action="{{ route('newShow') }}" method="post" class="form-horizontal tasi-form">
                                            @csrf


                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <input type="text" name="name_show" placeholder="أدخل اسم جهة العرض" id="f-name" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button class="btn btn-danger" type="submit">إضافة </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </section>
                            </div>
                        </div>

                        {{----}}
                {{--<p>form</p>--}}


                    {{--</section>--}}

                {{--</div>--}}

            {{--</div>--}}
            <!--state overview end-->




        </section>
    </section>
    <!--main content end-->


@stop