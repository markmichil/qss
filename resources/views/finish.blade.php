@extends('front')
@section('container')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            {{--<!--state overview start-->--}}
            {{--<div class="row state-overview">--}}
            {{--<div class="col-lg-12 col-sm-12">--}}

            {{--<section class="panel">--}}
            {{--<a href="#">--}}
            {{--<div class="symbol terques">--}}
            {{--<i class="icon-user"></i>--}}
            {{--<h4 style="color:white">{{__('key.mostkles')}} - DHL </h4>--}}
            {{--</div>--}}
            {{--<div class="value">--}}
            {{--<h1 class="count">--}}
            {{--0--}}
            {{--</h1>--}}
            {{--<p>{{__('key.shehada')}}</p>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--</section>--}}

            {{--</div>--}}

            {{--</div>--}}
            {{--<!--state overview end-->--}}

            {{--<!--state overview start-->--}}
            {{--<div class="row state-overview">--}}
            {{--<div class="col-lg-12 col-sm-12">--}}

            {{--<section class="panel">--}}
            {{--<a href="#">--}}
            {{--<div class="symbol terques">--}}
            {{--<i class="icon-user"></i>--}}
            {{--<h4 style="color:white">{{__('key.mostkles')}} - MOHAMED</h4>--}}
            {{--</div>--}}
            {{--<div class="value">--}}
            {{--<h1 class="count">--}}
            {{--0--}}
            {{--</h1>--}}
            {{--<p>{{__('key.shehada')}}</p>--}}
            {{--</div>--}}
            {{--</a>--}}
            {{--</section>--}}

            {{--</div>--}}

            {{--</div>--}}
            {{--<!--state overview end-->--}}
            {{------------------------------}}
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            <h1 style="color: #0088cc; text-align: center">{{__('key.Tshehada')}} - المنتهية </h1>
                        </header>
                        <table   class="table table-striped table-advance table-hover">
                            <thead>
                            <tr>
                                <th><i class="icon-bullhorn"></i> {{__('key.Nshehada')}}</th>
                                <th class="hidden-phone"><i class="icon-question-sign"></i> {{__('key.police')}}</th>

                                <th><i class="icon-bookmark"></i> {{__('key.mostkles')}}</th>
                                <th><i class=" icon-edit"></i> {{__('key.status')}}</th>
                                <th></th>

                            </tr>
                            </thead>
                            <tbody>


                            <tr>
                                <td>
                                    <a href="#">
                                        بل ايجيبت
                                    </a>

                                </td>
                                <td class="hidden-phone">9649296285</td>
                                <td> DHL </td>
                                <td><span class="label label-info label-mini">انتهت</span></td>
                                <td>
                                    <button class="btn btn-success btn-xs"><i class="icon-ok"> عرض التفاصيل</i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        جى ايه
                                    </a>
                                </td>
                                <td class="hidden-phone">8600680642</td>
                                <td>محمد DHL </td>
                                <td><span class="label label-warning label-mini">قيد التشغيل</span></td>
                                <td>
                                    <button class="btn btn-success btn-xs"><i class="icon-ok"> عرض التفاصيل</i></button>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="#">
                                        اسم شهادة
                                    </a>
                                </td>
                                <td class="hidden-phone">232350</td>
                                <td>أحمد </td>
                                <td><span class="label label-danger label-mini">مرتجع </span></td>
                                <td>
                                    <button class="btn btn-success btn-xs"><i class="icon-ok"> عرض التفاصيل</i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        بل ايجيبت
                                    </a>

                                </td>
                                <td class="hidden-phone">9649296285</td>
                                <td> DHL </td>
                                <td><span class="label label-info label-mini">انتهت</span></td>
                                <td>
                                    <button class="btn btn-success btn-xs"><i class="icon-ok"> عرض التفاصيل</i></button>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="#">
                                        جى ايه
                                    </a>
                                </td>
                                <td class="hidden-phone">8600680642</td>
                                <td>محمد DHL </td>
                                <td><span class="label label-warning label-mini">قيد التشغيل</span></td>
                                <td>
                                    <button class="btn btn-success btn-xs"><i class="icon-ok"> عرض التفاصيل</i></button>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <a href="#">
                                        اسم شهادة
                                    </a>
                                </td>
                                <td class="hidden-phone">232350</td>
                                <td>أحمد </td>
                                <td><span class="label label-danger label-mini">مرتجع </span></td>
                                <td>
                                    <button class="btn btn-success btn-xs"><i class="icon-ok"> عرض التفاصيل</i></button>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </section>
                </div>
            </div>

            {{---------------------------}}

        </section>
    </section>
    <!--main content end-->


@stop