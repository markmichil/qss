@extends('../front')
@section('container')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                       <h1 style="color: #0088cc; text-align: center">الموظفين </h1>
                    </header>
                    <table   class="table table-striped table-advance table-hover">
                        <thead>
                        <tr>
                            <th><i class="icon-bullhorn"></i> أسم الموظف </th>

                            <th><i class="icon-bullhorn"></i> التلفون</th>

                            <th><i class="icon-bullhorn"></i> تخصصاته</th>

                            <th><i class=" icon-edit"></i> كنترول </th>


                        </tr>
                        </thead>
                        <tbody>



                        <tr>
                            <td>
                                <a href="#">
                                   محمد
                                </a>

                            </td>
                            <td>
                                <a href="#">
                                   0121552215
                                </a>

                            </td>
                            <td>
                                <a id="add-without-image" class="btn btn-primary  btn-sm" href="javascript:;">صحة</a>
                                <a id="add-without-image" class="btn btn-info  btn-sm" href="javascript:;">تعليم</a>
                                <a id="add-without-image" class="btn btn-info  btn-sm" href="javascript:;">تعليم</a>
                                <a id="add-without-image" class="btn btn-info  btn-sm" href="javascript:;">تعليم</a>
                                <a id="add-without-image" class="btn btn-info  btn-sm" href="javascript:;">تعليم</a>

                            </td>

                            <td>
                                <button class="btn btn-danger btn-xs"><i class="icon-remove"> مسح</i></button>
                                <button class="btn btn-success btn-xs"><i class="icon-edit"> تعديل</i></button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="#">
                                   محمد
                                </a>

                            </td>
                            <td>
                                <a href="#">
                                   0121552215
                                </a>

                            </td>
                            <td>
                                <a id="add-without-image" class="btn btn-primary  btn-sm" href="javascript:;">صحة</a>
                                <a id="add-without-image" class="btn btn-info  btn-sm" href="javascript:;">تعليم</a>


                            </td>

                            <td>
                                <button class="btn btn-danger btn-xs"><i class="icon-remove"> مسح</i></button>
                                <button class="btn btn-success btn-xs"><i class="icon-edit"> تعديل</i></button>
                            </td>
                        </tr>



                        </tbody>
                    </table>
                </section>
            </div>
        </div>

        {{---------------------------}}

    </section>
</section>
<!--main content end-->


@stop