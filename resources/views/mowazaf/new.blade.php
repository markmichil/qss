@extends('../front')
@section('container')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">



                        <div class="row state-overview" dir="rtl">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                    إضافة موظف جديد
                                    </header>
                                    <div class="panel-body">
                                        <form role="form" method="post" action="{{route('newMowazaf')}}" class="form-horizontal tasi-form">
                                        @csrf


                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <input type="text" name="name" placeholder="أدخل اسم الموظف" id="f-name" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <input type="text" name="email" placeholder="البريد الإلكترونى " id="f-name" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <input type="text" name="phone" placeholder="أدخل التلفون " id="f-name" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <input type="text"  name="password" placeholder="أدخل الرقم السرى  " id="f-name" class="form-control">
                                                </div>

                                                <input type="hidden" type="hidden" name="type" value="2">

                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button class="btn btn-danger" type="submit">إضافة </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </section>
                            </div>
                        </div>

                        {{----}}
                {{--<p>form</p>--}}


                    {{--</section>--}}

                {{--</div>--}}

            {{--</div>--}}
            <!--state overview end-->




        </section>
    </section>
    <!--main content end-->


@stop