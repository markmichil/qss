@extends('front')
@section('container')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">
        <!--state overview start-->
        <div class="row state-overview">
            <div class="col-lg-12 col-sm-12">

                <section class="panel">
                    <a href="{{url('/mostklesById')}}">
                        <div class="symbol terques">
                            <i class="icon-user"></i>
                            <h4 style="color:white">{{__('key.mostkles')}} - DHL </h4>
                        </div>
                        <div class="value">
                            <h1 class="count">
                                0
                            </h1>
                            <p>{{__('key.shehada')}}</p>
                        </div>
                    </a>
                </section>

            </div>

        </div>
        <!--state overview end-->

        <!--state overview start-->
        <div class="row state-overview">
            <div class="col-lg-12 col-sm-12">

                <section class="panel">
                    <a href="{{url('/mostklesById')}}">
                        <div class="symbol terques">
                            <i class="icon-user"></i>
                            <h4 style="color:white">{{__('key.mostkles')}} - MOHAMED</h4>
                        </div>
                        <div class="value">
                            <h1 class="count">
                                0
                            </h1>
                            <p>{{__('key.shehada')}}</p>
                        </div>
                    </a>
                </section>

            </div>

        </div>
        <!--state overview end-->


    </section>
</section>
<!--main content end-->


@stop