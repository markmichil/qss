@extends('../front')
@section('container')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                       <h1 style="color: #0088cc; text-align: center">{{__('key.mostkles')}}</h1>
                    </header>
                    <table   class="table table-striped table-advance table-hover">
                        <thead>
                        <tr>
                            <th><i class="icon-bullhorn"></i> أسم المستخلص </th>

                            <th><i class=" icon-edit"></i> كنترول </th>


                        </tr>
                        </thead>
                        <tbody>



@if(count($show)>0)
                        @foreach($show as $sh)
                            <tr>
                                <td>
                                    <a href="#">
                                        {{$sh->customer_name}}
                                    </a>

                                </td>
                                <td>

                                    <a href="{{url('updateMostkles')}}/{{$sh->id}}" class="btn btn-success btn-xs"><i class="icon-pencil">تعديل</i></a>





                                </td>
                            </tr>
                        @endforeach
@else
                        <tr>
                            <td>لا يوجد بيانات</td>
                            <td>لا يوجد بيانات</td>

                        </tr>
    @endif
                        </tbody>
                    </table>
                </section>
            </div>
        </div>

        {{---------------------------}}
        {{$show->links()}}
    </section>
</section>
<!--main content end-->


@stop