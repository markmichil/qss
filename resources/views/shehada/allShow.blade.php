@extends('../front')
@section('container')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <h1 style="color: #0088cc; text-align: center">كل الشهادات </h1>
                    </header>
                    <table   class="table table-striped table-advance table-hover">
                        <thead>
                        <tr>
                            <th><i class="icon-bullhorn"></i> {{__('key.Nshehada')}}</th>
                            <th class="hidden-phone"><i class="icon-question-sign"></i> {{__('key.police')}}</th>

                            <th><i class="icon-bookmark"></i> {{__('key.mostkles')}}</th>
                            <th><i class=" icon-edit"></i> {{__('key.status')}}</th>
                            <th><i class=" icon-edit"></i> أرقم المحجوزة</th>


                            <th><i class=" icon-edit"></i> كنترول </th>

                        </tr>
                        </thead>

                        @if(count($mmm)>0)

                        <tbody>

                        @foreach($mmm as $shahada)
                        <tr>
                            <td><a href="#">{{$shahada->name}}</a></td>
                            <td class="hidden-phone">{{$shahada->policy}}</td>
                            <td> {{$shahada->customer->customer_name}} </td>

                            <td>
                                @if($shahada->finsih == 1 )
                                    <span class="label label-info label-mini">انتهت</span>
                                @elseif($shahada->status == 1)
                                    <span class="label label-warning label-mini">قيد التشغيل</span>
                                @elseif($shahada->problem == 1)
                                    <span class="label label-danger label-mini">مرتجع </span>
                                @endif
                            </td>
                            <td>

                                @foreach($shahada->mahgoza as $mahgoza)
                                    <p>- {{$mahgoza->mahgoza_number}}</p>
                                    @endforeach
                            </td>
                            <td>
                                <a href="{{url('deleteShehada')}}/{{$shahada->id}}" class="btn btn-danger btn-xs"><i class="icon-remove"> مسح</i></a>
                                <a href="{{url('updateShehada')}}/{{$shahada->id}}" class="btn btn-success btn-xs"><i class="icon-edit"> تعديل</i></a>

                            </td>

                        </tr>







                        @endforeach
                            @else
                            <tr>
                            <td>ادخل الشهادة اولا</td>
                            <td>لا يوجد بيانات</td>
                            <td>لا يوجد بيانات</td>
                            <td>لا يوجد بيانات</td>
                            <td>لا يوجد بيانات</td>

                            </tr>
                        </tbody>
                        @endif
                    </table>

                </section>
            </div>
        </div>

        {{$mmm->links()}}
    </section>
</section>
<!--main content end-->


@stop