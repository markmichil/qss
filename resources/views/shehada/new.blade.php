@extends('../front')
@section('container')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!--state overview start-->
            {{--<div class="row state-overview">--}}
                {{--<div class="col-lg-12 col-sm-12">--}}

                    {{--<section class="panel">--}}


                        <div class="row state-overview" dir="rtl">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                    إضافة شهادة جديدة
                                    </header>
                                    <div class="panel-body">
                                        <form role="form" method="post" action="{{route('newShehada')}}" class="form-horizontal tasi-form">
                                            @csrf


                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <input type="text" name="name" placeholder="أدخل اسم الشهادة " id="f-name" class="form-control">
                                                </div>

                                            </div>
                                            <div class="form-group">
                                              <div class="col-lg-offset-2 col-lg-10">
                                                  <label dir="rtl">مستخلص</label>
                                                <select  name ='customer_id' class="form-control m-bot15">
                                                    @if(count($mostkles)>0)
                                                        <option selected disabled>أختر</option>
                                                    @foreach($mostkles as $mostkl)
                                                    <option value="{{$mostkl->id}}">{{$mostkl->customer_name}}</option>
                                                    @endforeach
                                                        @else
                                                        <option selected disabled> لا يوجد مستخلص رجاء ادخل مستخلص اولا</option>
                                                    @endif



                                                </select>
                                                </div>

                                            </div>
                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <input type="text" name="policy" placeholder="أدخل رقم البوليصة " id="f-name" class="form-control">
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button class="btn btn-danger" type="submit">إضافة </button>
                                                </div>
                                            </div>

                                        </form>
                                    </div>
                                </section>
                            </div>
                        </div>

                        {{----}}
                {{--<p>form</p>--}}


                    {{--</section>--}}

                {{--</div>--}}

            {{--</div>--}}
            <!--state overview end-->




        </section>
    </section>
    <!--main content end-->


@stop