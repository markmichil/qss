@extends('../front')
@section('container')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">



                        <div class="row state-overview" dir="rtl">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                   تعديل جهة العرض للمحجوزة
                                    </header>
                                    <div class="panel-body">
                                        <form role="form"  action="{{ route('updatemahgoza')}}" method="post" class="form-horizontal tasi-form">
                                            @csrf


                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <input type="text"  disabled name="mahgoza_number" value="{{$mahgozas->mahgoza_number}}" id="f-name" class="form-control">

                                                </div>
                                                <input type="hidden" name="id" value="{{$mahgozas->id}}" id="f-name" class="form-control">
                                            </div>


                                            <div class="form-group">

                                                <div class="col-lg-10">
                                                    الجهة
                                                    <select multiple="[]" name="shows[]" class="form-control">

                                                            @foreach($shows as $show)
                                                                {{--@foreach($mahgozas->shows as $sho)--}}
                                                                    {{--@if($sho->id == $show->id)--}}
                                                                         {{--<option selected value="{{$show->id}}">{{$show->name_show}}</option>--}}
                                                                        {{--@else--}}
                                                                    <option  value="{{$show->id}}">{{$show->name_show}}
                                                                {{--@endif--}}


                                                            {{--@endforeach--}}
                                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <button class="btn btn-danger" type="submit">تعديل </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </section>
                            </div>
                        </div>

                        {{----}}
                {{--<p>form</p>--}}


                    {{--</section>--}}

                {{--</div>--}}

            {{--</div>--}}
            <!--state overview end-->




        </section>
    </section>
    <!--main content end-->


@stop