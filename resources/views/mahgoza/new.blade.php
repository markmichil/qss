@extends('../front')
@section('container')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!--state overview start-->
            {{--<div class="row state-overview">--}}
                {{--<div class="col-lg-12 col-sm-12">--}}

                    {{--<section class="panel">--}}


                        <div class="row state-overview" dir="rtl">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                        {{__('key.aMahgoza')}}
                                    </header>
                                    <div class="panel-body">
                                        <form role="form"  method="post" action="{{ route('newMahgoza') }}" class="form-horizontal tasi-form">
                                            @csrf


                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <input type="number"  name="mahgoza_number" placeholder="أدخل رقم المحجوز" id="f-name" class="form-control">
                                                </div>
                                            </div>







                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    <label dir="rtl">الشهادة</label>
                                                    <select  name ='shehada_id' class="form-control m-bot15">
                                                        @if(count($shehades)>0)
                                                            <option selected disabled>أختر</option>
                                                            @foreach($shehades as $shehade)
                                                                <option value="{{$shehade->id}}">{{$shehade->name}}</option>
                                                            @endforeach
                                                        @else
                                                            <option selected disabled> لا يوجد شهادة  رجاء ادخل الشهادة اولا</option>
                                                        @endif



                                                    </select>
                                                </div>

                                            </div>





                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    @if(count($shehades)>0)
                                                    <button class="btn btn-danger" type="submit">إضافة </button>
                                                        @else
                                                        <button  disabled class="btn btn-danger" type="submit">إضافة </button>
                                                        @endif
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </section>
                            </div>
                        </div>

                        {{----}}
                {{--<p>form</p>--}}


                    {{--</section>--}}

                {{--</div>--}}

            {{--</div>--}}
            <!--state overview end-->




        </section>
    </section>
    <!--main content end-->


@stop