@extends('../front')
@section('container')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <!--state overview start-->
            {{--<div class="row state-overview">--}}
                {{--<div class="col-lg-12 col-sm-12">--}}

                    {{--<section class="panel">--}}


                        <div class="row state-overview" dir="rtl">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                   تخصيص المحجوزة للجهة
                                    </header>
                                    <div class="panel-body">
                                        <form role="form" method="post"  action="{{route('customeMahgoza')}}" class="form-horizontal tasi-form">
                                            @csrf
                                            <div class="form-group">
                                            <div class="col-lg-10">
                                                رقم المحجوزة
                                                <select  name="mahgoza" class="form-control m-bot15">
                                                    @if(count($mahgozas)>0)
                                                        @foreach($mahgozas as $mahgoza)
                                                        <option value="{{$mahgoza->id}}">{{$mahgoza->mahgoza_number}}</option>
                                                        @endforeach

                                                @else
                                                        <option> لا يوجد ارقام ادخل رقم المحجوزة اولا </option>
                                                    @endif
                                                </select>
                                            </div>

                                            </div>
                                            <div class="form-group">

                                            <div class="col-lg-10">
                                                الجهة
                                                <select multiple="[]" name="shows[]" class="form-control">
                                                    @if(!(count($mahgozas)>0) && (count($shows)>0))
                                                    <option disabled>ادخل اولا رفم المحجوزة </option>
                                                        @else
                                                            @foreach($shows as $show)
                                                        <option value="{{$show->id}}">{{$show->name_show}}</option>
                                                        @endforeach
                                                        @endif
                                                </select>
                                            </div>
                                            </div>



                                            <div class="form-group">
                                                <div class="col-lg-offset-2 col-lg-10">
                                                    @if(!(count($mahgozas)>0) && (count($shows)>0))
                                                    <button class="btn btn-danger" disabled type="submit">إضافة </button>

                                                        @else
                                                        <button class="btn btn-danger"  type="submit">إضافة </button>
                                                        @endif
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </section>
                            </div>
                        </div>

                        {{----}}
                {{--<p>form</p>--}}


                    {{--</section>--}}

                {{--</div>--}}

            {{--</div>--}}
            <!--state overview end-->




        </section>
    </section>
    <!--main content end-->


@stop