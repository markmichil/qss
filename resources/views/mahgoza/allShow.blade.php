@extends('../front')
@section('container')
        <!--main content start-->
<section id="main-content">
    <section class="wrapper">

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                       <h1 style="color: #0088cc; text-align: center">أرقام المحجوزة </h1>
                    </header>
                    <table   class="table table-striped table-advance table-hover">
                        <thead>
                        <tr>
                            <th><i class="icon-bullhorn"></i> رقم المحجوزة  </th>

                            <th><i class=" icon-edit"></i> الجهات المختارة </th>
                            <th><i class=" icon-edit"></i> كنترول </th>


                        </tr>
                        </thead>
                        <tbody>

                @foreach($mahgozas as $mahgoza)
                        <tr>
                            <td>
                                <a href="#">
                                  {{$mahgoza->mahgoza_number}}
                                </a>

                            </td>
                            <td>
                             @foreach($mahgoza->shows as $show)

                                <p> - {{$show->name_show}}</p>
                               @endforeach





                            </td>
                            <td>


                                <a href="{{url('updatemahgoza')}}/ {{$mahgoza->id}}"  class="btn btn-primary btn-xs"><i class="icon-pencil">تعديل</i></a>




                            </td>
                        </tr>

                        @endforeach


                        </tbody>

                    </table>
                </section>
            </div>
        </div>
        {{ $mahgozas->links() }}
        {{---------------------------}}

    </section>
</section>
<!--main content end-->


@stop