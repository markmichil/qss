<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
//*/
//
//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/','HomeController@index');
Route::get('login','HomeController@login');

Route::get('/mostkles','HomeController@mostkles');
Route::get('/mostklesById','HomeController@mostklesById');
Route::get('/shehada','HomeController@shehada');
Route::get('/finish','HomeController@finish');
Route::get('/return','HomeController@returnn');
Route::get('/still','HomeController@still');

/********************SIDE MENU BAR******************************/
Route::get('newShow','admin\ShowController@newShow');
Route::post('newShow','admin\ShowController@addShow')->name('newShow');
Route::get('allShow','admin\ShowController@allShow');
Route::get('updateShow/{id}','admin\ShowController@updateShow')->name('updateShow');
Route::post('updateShow','admin\ShowController@DoupdateShow')->name('updateShow');

/***************************End show**************************************/

Route::get('newMostkles','admin\MostklesController@newMostkles');
Route::post('newMostkles','admin\MostklesController@addMostkles')->name('newMostkles');
Route::get('allMostkles','admin\MostklesController@allMostkles');
Route::get('updateMostkles/{id}','admin\MostklesController@updateMostkles')->name('updateMostkles');
Route::post('updateMostkles','admin\MostklesController@DoupdateMostkles')->name('updateMostkles');
/***************************End mostkles******************* *******************/



Route::get('newMahgoza','admin\MahgozaController@newMahgoza')->name('newMahgoza');
Route::post('newMahgoza','admin\MahgozaController@addMahgoza')->name('newMahgoza');
Route::get('customeMahgoza','admin\MahgozaController@customeMahgoza');
Route::post('customeMahgoza','admin\MahgozaController@DocustomeMahgoza')->name('customeMahgoza');
Route::post('all','admin\MahgozaController@all')->name('all');
Route::get('allmahgoza','admin\MahgozaController@all')->name('allmahgoza');
Route::get('mahgozas','admin\MahgozaController@mahgozas')->name('mahgozas');
Route::get('updateMahgozasShehada/{id}','admin\MahgozaController@updateMahgozasShehada')->name('updateMahgozasShehada');
Route::post('updateMahgozasShehada','admin\MahgozaController@DoupdateMahgozasShehada')->name('updateMahgozasShehada');
Route::get('updatemahgoza/{id}','admin\MahgozaController@updateMahgoza')->name('updatemahgoza');
Route::post('updatemahgoza','admin\MahgozaController@DoupdateMahgoza')->name('updatemahgoza');





/**************************************Shehada***************************************************************/

Route::get('newShehada','admin\ShehadaController@newShehada');
Route::post('newShehada','admin\ShehadaController@addShehada')->name('newShehada');
Route::get('allShehada','admin\ShehadaController@allShehada');



/*********************************************Mowazaf***********************************************************/

Route::get('newMowazaf','admin\MowazafController@newMowazaf');
Route::post('newMowazaf','admin\MowazafController@create')->name('newMowazaf');
Route::get('allMowazaf','admin\MowazafController@allMowazaf');
Route::get('custome','admin\MowazafController@custome');
Route::get('job','admin\MowazafController@job');

/****************************************************************************/




Route::get('notRecive','SideController@notRecive');
Route::get('working','SideController@working');

Route::get('dhl','DHLController@index')->name('dhl');


/*******************END MENU BAR ***************************************/

/**************************DHL laravek*******************************************/


Auth::routes();

Route::post('/login/custom', [
    'uses'=>'LoginController@login',
    'as'=>'login.custom'
]);
Route::get('/logout/custom', [
    'uses'=>'LoginController@logout',
    'as'=>'logout.custom'
]);

Route::get('/logout/custo', [
    'uses'=>'HomeController@logout',
    'as'=>'logout.custo'
]);


Route::get('/dashboard', 'HomeController@index')->name('dashboard');


Route::get('/user','user\UserHomeController@index')->name('user');
